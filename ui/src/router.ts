import Vue from 'vue'
import Router from 'vue-router'
import MainRoute from './modules/main/main.route';
import Home from './views/Home.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      component: () => import(/* webpackChunkName: "Login" */ './modules/main/main.vue'),
      children: MainRoute()
    }
  ]
})
